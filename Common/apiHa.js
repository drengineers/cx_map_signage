import $axios from 'axios';
import router from 'vue-router'
import confirm from '@/util/confirm'

$axios.interceptors.request.use(function (config) {
  if (localStorage.token) {
    let token = localStorage.token;
    config.headers.CX_TOKEN = token;
  }
  if (localStorage.meetingIdMap) {
    let meetingId = localStorage.meetingIdMap;
    config.headers.CX_MEETING = meetingId;
  }
  return config;
}, function (error) {
  console.log("==== error=====", error)
  // 对请求错误做些什么
  return Promise.reject(error);
});

$axios.interceptors.response.use(function (response) {
  let that = this;
  // 处理响应数据
  if (response.status === 200) {
    return Promise.resolve(response);
  } else {
    return Promise.reject(response);
  }
}, function (error) {
  if (error.response.status === 401) {
    if (!(window.location.href.indexOf('loginEmail') > -1)) {
      confirm({title: "Confirmation", message: 'Unauthorized', show: true}).then(() => {
        //用户点击确认后执行
        if (process.env.NODE_ENV === 'production') {
          window.location.href = '/mapSpace#/pages/loginEmail';
        }else{
          window.location.href = '/#/pages/loginEmail';
        }
        localStorage.setItem("Unauthorized", "false");
      }).catch(err => {
        if (process.env.NODE_ENV === 'production') {
          window.location.href = '/mapSpace#/pages/loginEmail';
        }else{
          window.location.href = '/#/pages/loginEmail';
        }
        localStorage.setItem("Unauthorized", "false");
      })
    }
  }
  // if (error.response.status === 401) {
  //   console.log("====00000000000000000000 =====",)
  //   if (!(window.location.href.indexOf('loginEmail') > -1)) {
  //     if (localStorage.getItem("Unauthorized") == "true") {
  //       console.log("====11111111 =====",)
  //       return
  //     } else {
  //       console.log("====22222222 =====",)
  //       localStorage.setItem("Unauthorized", "true");
  //       confirm({title: "Confirmation", message: 'Unauthorized', show: true}).then(() => {
  //         //用户点击确认后执行
  //         window.location.href = '/#/pages/loginEmail';
  //         localStorage.setItem("Unauthorized", "false");
  //       }).catch(err => {
  //         window.location.href = '/#/pages/loginEmail';
  //         localStorage.setItem("Unauthorized", "false");
  //       })
  //     }
  //   }
  // }
  // 处理响应失败
  return Promise.reject(error);
});


