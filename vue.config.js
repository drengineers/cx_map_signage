// vue.config.js
// const path = require('path');
// const webpack = require('webpack');
// function resolve(dir){
//   return path.join(__dirname, dir);
// };

// this.getClient

// let isClorox = true;
// let customCss = {};
// if(isClorox){
//   customCss = {
//     loaderOptions: {
//       scss: {
//         data: `
//           @import "@/assets/css/globalClorox.scss";
//         `
//       }
//     }
//   }
// }else{
//   customCss = {
//     loaderOptions: {
//       scss: {
//         data: `
//           @import "@/assets/css/global.scss";
//         `
//       }
//     }
//   }
// }

// let target = "https://7-1-6-dot-cx-app-campus.appspot.com/campus";
// let target = "https://7-1-6-dot-cx-app-clorox.uc.r.appspot.com/cms";
const target = "https://7-1-40-dot-cx-app-campus.appspot.com/"

module.exports = {
  // css: customCss,
  publicPath: process.env.NODE_ENV === 'production'
    ? '/mapSpace/'
    : './',
  assetsDir: "static",
  devServer: {
    port: 8081,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    hotOnly: true, // 热更新
    proxy: {
      "/cms": {
        // target: "https://cx-app-aruba.appspot.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-warnermedia.uc.r.appspot.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://wmredcarpet.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://stage-dot-cx-app-adobe.appspot.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://dev-dot-cx-app-warnermedia.appspot.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-natwest.uc.r.appspot.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://stage-dot-cx-app-campus.appspot.com/cms", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-natwestcampus.nw.r.appspot.com//cms", // 设置你调用的接口域名和端口号 别忘了加http
        target: target+'cms', // 设置你调用的接口域名和端口号 别忘了加http
        // target:target,
        changeOrigin: true, // 这里设置是否跨域
        pathRewrite: {
          "^/cms": ""
        }
      },
      "/files": {
        // target: "https://cx-app-aruba.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-warnermedia.uc.r.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://wmredcarpet.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://stage-dot-cx-app-adobe.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://dev-dot-cx-app-warnermedia.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-natwest.uc.r.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://stage-dot-cx-app-campus.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-natwestcampus.nw.r.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        // target: "https://cx-app-campus.appspot.com/files", // 设置你调用的接口域名和端口号 别忘了加http
        target: target+'files', // 设置你调用的接口域名和端口号 别忘了加http
        // target:target,
        changeOrigin: true, // 这里设置是否跨域
        pathRewrite: {
          "^/files": ""
        }
      },
      "/m": {
        // target: "https://cx-app-aruba.appspot.com/m",
        // target: "https://cx-app-warnermedia.uc.r.appspot.com/m",
        // target: "https://wmredcarpet.com/m",
        // target: "https://stage-dot-cx-app-adobe.appspot.com/m",
        // target: "https://dev-dot-cx-app-warnermedia.appspot.com/m",
        // target: "https://cx-app-natwest.uc.r.appspot.com/m",
        // target: "https://stage-dot-cx-app-campus.appspot.com/m",
        // target: "https://cx-app-natwestcampus.nw.r.appspot.com/m",
        target: target+'m',
        // target:target,
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          "^/m": ""
        }
      }
    }
  },

  pwa: {
    // iconPaths: {
    //   favicon32: "./img/favicon/redHat.ico",
    //   favicon16: "./img/favicon/redHat.ico",
    //   appleTouchIcon: "./img/favicon/redHat.ico",
    //   maskIcon: "./img/favicon/redHat.ico",
    //   msTileImage: "./img/favicon/redHat.ico"
    // }
  },
  parallel: false,
  chainWebpack: config => {
    config.output.globalObject('this')

  },
  configureWebpack: config => {
    config.module.rules.push({
      test: /\.worker.js$/,
      use: {
        loader: 'worker-loader',
        options: {inline: true, name: 'workerName.[hash].js'}
      }
    })
    //生产环境取消 console.log
    // if (process.env.NODE_ENV === 'production') {
    //   config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    // }
  },
};
