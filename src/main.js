import Vue from 'vue'
import axios from "axios";
import VueCountryCode from "vue-country-code";
import vueCountryRegionSelect from 'vue-country-region-select'
import Vtip from 'vtip'
import 'vtip/lib/index.min.css'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import './plugins/vue-world-map'
import vuetify from './plugins/vuetify'
import Message from './components/Message/index.js'

import fabric from 'fabric'
// import Vuetify from 'vuetify/lib'
// import { ripple } from 'vuetify/lib/directives'
import i18n from './i18n'

// 引用公共js
import common from "../Common/common.js";
import "../Common/apiHa"
import {get, post} from '../Common/api';

import confirm from './util/confirm'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './assets/iconfont/iconfont.css'
// import './assets/css/global.scss'
import './assets/css/all.scss'

import Swiper, {Pagination, Navigation, Autoplay} from 'swiper' // js 模块
import 'swiper/swiper-bundle.css' // css 模块
import VueWorker from 'vue-worker'
import moment from 'moment'
// 防止v-html的xss攻击
import VueDOMPurifyHTML from 'vue-dompurify-html'
Vue.use(VueDOMPurifyHTML)

Vue.prototype.moment = moment;
Vue.use(VueWorker);
Swiper.use([Pagination, Navigation, Autoplay]);

Vue.prototype.$axios = axios;

Vue.prototype.$confirm = confirm;
//将方法挂载到Vue原型上
Vue.prototype.get = get;
Vue.prototype.post = post;


Vue.config.productionTip = false;
Vue.prototype.bus = new Vue();
Vue.use(common);
Vue.prototype.$message = Message;
Vue.use(VueCountryCode);
Vue.use(vueCountryRegionSelect);
Vue.use(Vtip.directive);

// 工具函数调用
Vue.prototype.$tip = Vtip.tip;
// Vue.use(Vuetify, {
//   directives: {
//     Ripple: ripple
//   }
// });
Vue.use(Viewer);

Vue.use(fabric);
Vue.prototype.getClientData = function () {
  return this.$axios({
    method: 'GET',
    url: 'cms/config',
  })
};
Vue.prototype.getClient = function () { //changeData是函数名
  const _this = this;
  axios
    .get("cms/config")
    .then((response) => {
      console.log("==== 555555=====",)
      console.log("====response.data.Client =====",response.data.Client)
      if (response.data.Client == 'Clorox') {
        import ('@/assets/css/globalClorox.scss');
        localStorage.setItem("client",'Clorox');
      }else if(response.data.Client == 'WarnerMedia'){
        import ('@/assets/css/globalWarnerMedia.scss');
        localStorage.setItem("client",'WarnerMedia');
      }else if(response.data.Client == 'Aruba'){
        import ('@/assets/css/globalAruba.scss');
        localStorage.setItem("client",'Aruba');
      }else if(response.data.Client == 'HPE'||response.data.Client == 'HPE Campus'){
        import ('@/assets/css/globalHpe.scss');
        localStorage.setItem("client",'Aruba');
      }  else {
        import ('@/assets/css/global.scss')
      }
      _this.$store.commit("setClient", response.data.Client);
      _this.version = response.data.Client;

      // console.log(_this.$store.state.client === _this.$store.state.version.ORSTED);
      // console.log(_this.$store);
      // _this.$forceUpdate();
    })
    .catch((err) => {
      console.log(err);
    });
};

Vue.prototype.getConfig = function () { //changeData是函数名
  const _this = this;
  axios
    .get("m/config")
    .then((response) => {
      // console.log('version', response);
      localStorage.config = JSON.stringify(response.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
Vue.prototype.getConfigReturn = function () { //changeData是函数名
  const _this = this;
  return axios
    .get("m/config")
};
Vue.prototype.getUserInfo = function (token) { //changeData是函数名
  localStorage.stoken = token;
  const _this = this;
  let url = "m/profile/?token=" + token;
  axios
    .get(url)
    .then((response) => {
      // console.log('profile', response);
      localStorage.userEmail = response.data.email;
      localStorage.userId = response.data.userId;
      localStorage.userName = response.data.firstName + ' ' + response.data.lastName;
      localStorage.image = response.data.image;
      // console.log(123,token);
      // _this.$router.push({
      //   path: "/mec",
      // }).catch(err => {
      //   console.log(err);
      // });
    })
    .catch((err) => {
      console.log(err);
    });
};
Vue.prototype.getUserInfo = function (token) { //changeData是函数名
  localStorage.stoken = token;
  const _this = this;
  let url = "m/profile/?token=" + token;
  axios
    .get(url)
    .then((response) => {
      // console.log('profile', response);
      // localStorage.userEmail = response.data.email;
      localStorage.userId = response.data.userId;
      localStorage.userName = response.data.firstName + ' ' + response.data.lastName;
      localStorage.image = response.data.image;
      // console.log(123,token);
      // _this.$router.push({
      //   path: "/mec",
      // }).catch(err => {
      //   console.log(err);
      // });
    })
    .catch((err) => {
      console.log(err);
    });
};


Viewer.setDefaults({
  Options: {
    "inline": true,
    "button": true,
    "navbar": true,
    "title": true,
    "toolbar": true,
    "tooltip": true,
    "movable": true,
    "zoomable": true,
    "rotatable": true,
    "scalable": true,
    "transition": true,
    "fullscreen": true,
    "keyboard": true,
    "url": "data-source"
  }
});
Vue.filter('moment', (value, formatString) => {
  formatString = formatString || 'MMMM DD, YYYY';
  return moment(value).format(formatString);
});

router.beforeEach((to, from, next) => {
  // if(localStorage.stoken != ''){
  //   console.log(to.query.token,111);
  //   if(to.query.token){
  //     Vue.prototype.getUserInfo(to.query.token);
  //     // next();
  //     this.$router.push({
  //       path: "/mec",
  //     }).catch(err => {
  //       console.log(err);
  //     });
  //   }
  // }else{
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // 这里判断用户是否登录，验证本地存储是否有token
    if (!localStorage.token) { // 判断当前的token是否存在
      if (to.query.token) {
        next()
      } else {
        next({
          path: '/pages/loginEmail',
          query: {redirect: to.fullPath}
        });
      }

      // next()
    } else {
      if (to.name === 'Dashboard') {
        next({
          path: '/mec',
          query: {redirect: to.fullPath}
        });
      } else {
        next()
      }
    }
  } else {
    next() // 确保一定要调用 next()
  }
  // }

})

router.afterEach((to, from, next) => {
  // console.log("ga",ga)
  // ga('set', 'page', to.path)
  // ga('send', 'pageview')
  // console.log("to:", to);
  let eventCategory = to.path;
  let eventLabel = to.name ? to.name : to.path;
  let id = localStorage.getItem("googleAnaId");
  // window.gtag('config', 'page_view', {
  //   page_title: eventLabel,
  //   page_path: eventCategory,
  //   send_to: id
  //   // send_to: this.GA_TRACKING_ID
  // })

  if (localStorage.client == "NatWest") {
    router.afterEach((to, from, next) => {
      if(window.gtag){
        window.gtag('event', 'page_view', {
          event_category: eventLabel,
          event_label: eventCategory,
          send_to: id
          // send_to: this.GA_TRACKING_ID
        })
      }

    })
  }

})
// if (localStorage.client == "NatWest") {
//   router.afterEach((to, from, next) => {
//     console.log("to:",to)
//     // window.gtag('event', eventAction, {
//     //   event_category: eventCategory,
//     //   event_label: eventLabel,
//     //   value: eventValue,
//     //   send_to: this.GA_TRACKING_ID
//     // })
//   })
// }


new Vue({
  router,
  store,
  vuetify,
  axios,
  i18n,
  data: function () {
    return {
      ORDERID: 'PLDxxxxxx0001',
    }
  },
  created() {
    Vue.prototype.getClient();
    Vue.prototype.getConfig();
    const _this = this;
    let url = "";
    url = "/m/campus/maps";
    if (localStorage.meetingIdMap) {
      url += `?meetingId=${localStorage.meetingIdMap}`;
    }
    axios
      .get(url)
      .then((response) => {
        localStorage.mapData = JSON.stringify(response.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  render: h => h(App)
}).$mount('#app')
